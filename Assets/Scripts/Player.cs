using UnityEngine;

public class Player : MonoBehaviour
{
    private float horizontal;
    private float speed = 8f;
    private float jumpingPower = 5f;
    private bool isFacingRight = true;

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Animator animator;

    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        if (horizontal != 0) animator.SetInteger("State", 1);
        else animator.SetInteger("State", 0);

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            GetComponent<AudioSource>().Play();
            rb.velocity = new Vector2(rb.velocity.x, jumpingPower);
            animator.SetInteger("State", 2);
           
        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }
        this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, 0,this.transform.rotation.w);
        Flip();
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }

    private bool IsGrounded()
    {
        bool checker =  Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
        if (checker) animator.SetInteger("State", 0); 
        return checker;
    }

    private void Flip()
    {
        if (isFacingRight && horizontal < 0f || !isFacingRight && horizontal > 0f)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;   
            transform.localScale = localScale;
        }
    }

  

    private void OnBecameInvisible()
    {
        GameObject.Find("Canvas").GetComponent<Canvas>().GoToEndScene(0,"GameScene");
    }


}