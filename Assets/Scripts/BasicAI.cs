using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAI : MonoBehaviour
{
    private float horizontal;
    private bool isFacingRight = true;
    private float speed = 2f;
    private float jumpingPower = 3f;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Animator animator;
    [SerializeField] float TotalSurvivalTime = 60f;
    float timeLeft;
    public float SurvivalTime { get { return timeLeft; } }

    [SerializeField] float distVert;

    // Start is called before the first frame update
    void Start()
    {
        timeLeft = TotalSurvivalTime;
        horizontal = 1;
        animator.SetInteger("State", 1);
    }

    // Update is called once per frame
    void Update()
    {
        checkFlip();
        move();
        timeLeft -= Time.deltaTime;
    }
    private void move()
    {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }

    private void checkFlip()
    {
        Vector3 atras = new Vector3(transform.position.x - distVert, transform.position.y - 1.5f);
        Vector3 delante = new Vector3(transform.position.x + distVert, transform.position.y - 1.5f);
        Debug.DrawRay(atras, transform.up, Color.red);
        Debug.DrawRay(delante, transform.up, Color.green);


        RaycastHit2D hit = Physics2D.Raycast(atras, transform.up);
        this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, 0, this.transform.rotation.w);

        if (hit.collider == null && hit.collider!=this)
        {
            Flip();
        }
    
        hit = Physics2D.Raycast(delante, transform.up);
        if (hit.collider == null && hit.collider != this)
        {
            Flip();
        }
    
    }

    private void Flip()
    {
       
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
            horizontal*=-1f;
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Fireball")
        {
            Flip();
        }
    }

}
