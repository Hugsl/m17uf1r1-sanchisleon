using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fireball : MonoBehaviour
{


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") if (SceneManager.GetActiveScene().name == "GameScene") GameObject.Find("Canvas").GetComponent<Canvas>().GoToEndScene(0, "GameScene"); else GameObject.Find("Canvas").GetComponent<Canvas>().GoToEndScene(1, "ThrowFireball");  //Needs implementation 

        if (collision.tag == "Ground")Destroy(this.gameObject);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(this.gameObject);
    }
    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
