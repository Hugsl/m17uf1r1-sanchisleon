using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Canvas : MonoBehaviour
{

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == "EndScene")
        {
            if (PlayerPrefs.GetInt("result") == 1) {
                GameObject.Find("wintext").GetComponent<TMP_Text>().text = "You win"; 
            }
            else GameObject.Find("wintext").GetComponent<TMP_Text>().text = "You loose";
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name != "EndScene")
        {
            if (GameObject.Find("Spawner"))
            {
                GetComponentInChildren<TextMeshProUGUI>().text = GameObject.Find("Spawner").GetComponent<Spawner>().SurvivalTime.ToString("F0");

                if (GameObject.Find("Spawner").GetComponent<Spawner>().SurvivalTime < 0) GoToEndScene(0, "GameScene");
            }
            else
            {

                GetComponentInChildren<TextMeshProUGUI>().text = GameObject.Find("Player").GetComponent<BasicAI>().SurvivalTime.ToString("F0");
                if (GameObject.Find("Player").GetComponent<BasicAI>().SurvivalTime < 0) GoToEndScene(0, "ThrowFireball");
            }
        }

     


    }

    public void GoToInitialScene()
    {
        SceneManager.LoadScene("Start");
    }
    public void GoToDodgeScene()
    {
        SceneManager.LoadScene("GameScene");
    }
    public void GoToShootScene()
    {
        SceneManager.LoadScene("ThrowFireball");
    }
    public void GoToEndScene(int result,string lastScene)
    {
        PlayerPrefs.SetString("lastScene", lastScene);
        PlayerPrefs.SetInt("result", result);
        SceneManager.LoadScene("EndScene");
    }

public void GoToLast()
    {
        SceneManager.LoadScene(PlayerPrefs.GetString("lastScene"));
    }
}
