using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
   [SerializeField] GameObject fireball;
    
    // Spawn fire ball time
    
    float cooldown;
    [SerializeField]float  totalCooldown;
   
    // Game Survival Time
    
    
    [SerializeField] float TotalSurvivalTime = 60f;
    float timeLeft;
    public float SurvivalTime { get { return timeLeft; } }


    private void Start()
    {
        timeLeft = TotalSurvivalTime;
        cooldown = totalCooldown;
    }
    void Update()
    {
        if (timeLeft > 0) Spawn();
        else GameObject.Find("Canvas").GetComponent<Canvas>().GoToEndScene(1, "GameScene");

    }

    private void Spawn()
    {
        if (cooldown >= 0)
        {
            cooldown -= Time.deltaTime;
        }
        else
        {


            Vector2 randomSpawnPosition = new Vector2(Random.Range(-5, 5), 5);
            Instantiate(fireball, randomSpawnPosition, Quaternion.identity);
            cooldown = totalCooldown;
        }

        timeLeft -= Time.deltaTime;
    }
  
}
