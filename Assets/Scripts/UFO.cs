using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFO : MonoBehaviour
{
    private float horizontal;
    private float speed = 8f;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] GameObject fireball;
    [SerializeField] Camera camara;
    [SerializeField] IEnumerator a = null;


    // Update is called once per frame

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }
    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, 0, this.transform.rotation.w);


        if (Input.GetButtonDown("Jump") )
        {
            Instantiate(fireball,this.transform.position,new Quaternion());
        }
        
        
        if (Input.GetMouseButtonDown(0))
        {
            if (a == null)
            {
                a = shoot();
                StartCoroutine(a);
            }

        }
    }

        IEnumerator shoot()
        {
        var position = camara.ScreenToWorldPoint(Input.mousePosition);

        // position[0]; X
        position[1] = 5; //Y
        position[2] = 0; //Z
        Instantiate(fireball, position, new Quaternion(0f, 0f, 0f, 0f));

        yield return new WaitForSeconds(1f);
        a = null;
    
    }

}
